<?php

namespace DesarrolloBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
* @Annotation
*/
class ExisteExpediente extends Constraint
{
	public $message = 'El expediente no existe.';

	public function validatedBy()
    {
        return 'validator_existe_expediente';
    }

	public function getTargets()
	{
	    return self::CLASS_CONSTRAINT;
	}
}
