<?php

namespace DesarrolloBundle\Validator\Constraints;

use Symfony\Component\Validator\ConstraintValidator;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;

/**
* CuilValidator
*/
class CuilValidator extends ConstraintValidator
{
    public function validate($object, Constraint $constraint)
    {
        $cuil = $object->getCuil();

        if (strlen($cuil) == 11) {
            $cadena = str_split($cuil);

            $result = $cadena[0] * 5;
            $result += $cadena[1] * 4;
            $result += $cadena[2] * 3;
            $result += $cadena[3] * 2;
            $result += $cadena[4] * 7;
            $result += $cadena[5] * 6;
            $result += $cadena[6] * 5;
            $result += $cadena[7] * 4;
            $result += $cadena[8] * 3;
            $result += $cadena[9] * 2;

            $div = intval($result / 11);
            $resto = $result - ($div * 11);
            $valido = true;

            if ($resto == 0) {
                if ($resto != $cadena[10]) {
                    $valido = false;
                }
            } elseif ($resto == 1) {
                if ($cadena[10] == 9 && $cadena[0] == 2 && $cadena[1] == 3) {
                    $valido = true;
                } elseif ($cadena[10] == 4 && $cadena[0] == 2 && $cadena[1] == 3) {
                    $valido = true;
                } else {
                    $valido = false;
                }
            } elseif ($cadena[10] == (11-$resto)) {
                $valido = true;
            } else {
                $valido = false;
            }
            if (!$valido) {
                $this->context->addViolationAtSubPath('cuil', $constraint->message);
            }
        } else {
            $this->context->addViolationAtSubPath('cuil', $constraint->lenMessage);
        }
    }
}
