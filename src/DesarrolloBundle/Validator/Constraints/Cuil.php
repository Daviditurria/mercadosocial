<?php

namespace DesarrolloBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
* @Annotation
*/
class Cuil extends Constraint
{
	public $message = 'El cuil no es válido.';
	public $lenMessage = 'El cuil debe tener 11 dígitos.';

	public function validatedBy()
    {
        return 'validator_cuil';
    }

	public function getTargets()
	{
	    return self::CLASS_CONSTRAINT;
	}
}
