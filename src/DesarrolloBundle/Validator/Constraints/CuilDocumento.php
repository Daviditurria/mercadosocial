<?php

namespace DesarrolloBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
* @Annotation
*/
class CuilDocumento extends Constraint
{
	public $message = 'El cuil no es válido.';
	public $lenMessage = 'El cuil debe tener 11 dígitos.';
	public $docMessage = 'El cuil no pertenece a esta persona.';

	public function validatedBy()
    {
        return 'validator_cuil_documento';
    }

	public function getTargets()
	{
	    return self::CLASS_CONSTRAINT;
	}
}
