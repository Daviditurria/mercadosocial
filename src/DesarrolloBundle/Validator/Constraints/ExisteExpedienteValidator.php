<?php

namespace DesarrolloBundle\Validator\Constraints;

use Symfony\Component\Validator\ConstraintValidator;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;

/**
* ExisteExpedienteValidator
*/
class ExisteExpedienteValidator extends ConstraintValidator
{
	private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function validate($object, Constraint $constraint)
    {
        $sql = 'SELECT expedientes.fnc_existe_exp(:ente, :nro, :anio, :alcance) AS cant FROM dual';
        $stmt = $this->em
                ->getConnection()
                ->prepare($sql);
        $stmt->bindValue(':ente', $object->getExpeEnte());
        $stmt->bindValue(':nro', $object->getExpeNro());
        $stmt->bindValue(':anio', $object->getExpeAnio());
        $stmt->bindValue(':alcance', $object->getExpeAlcance());

        $stmt->execute();
        $existeExpediente = $stmt->fetchColumn();

        if ($existeExpediente == 0){
            $this->context->addViolationAtSubPath('expeEnte', $constraint->message);
        }
    }
}
