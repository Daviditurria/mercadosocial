<?php

namespace DesarrolloBundle\Annotations;

/**
* @Annotation
*/
class Breadcrumb
{
	public $title;

	public $route;

	public $params;

	public $dinamic;

	public $titleParams;
}
