<?php

namespace DesarrolloBundle\Annotations\Listener;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Util\ClassUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpFoundation\Request;
use \DesarrolloBundle\Util\Breadcrumb\Breadcrumb;

/**
* AnnotationListener
*/
class AnnotationListener
{
	protected $reader;
	protected $request;
	protected $breadcrumbService;

	private $breadcrumbAnnotation;
    private $breadcrumb;
    private $breadcrumbAdd;
    private $defaultRoute;

	/**
	 * @var AnnotationReader $reader
     * @var RequestStack $request
     * @var Breadcrumb $breadcrumbService
	 *
	 */
	function __construct($reader, $request, $breadcrumbService)
	{
		$this->reader = $reader;
		$this->request = $request->getCurrentRequest();
		$this->breadcrumbService = $breadcrumbService;

		$this->breadcrumbAnnotation = "\DesarrolloBundle\Annotations\Breadcrumb";
        $this->breadcrumb = array();
        $this->breadcrumbAdd = array();
	}

    public function setConfig($defaultRoute)
    {
        $this->defaultRoute = $defaultRoute;
    }

	public function onKernelController(FilterControllerEvent $event)
	{
		$controller = $event->getController();
        $attributes = $this->request->attributes->all();

		if (!is_array($controller)) {
            return;
        }

        list($controllerObject, $methodName) = $controller;

        $className = ClassUtils::getClass($controllerObject);
        $object    = new \ReflectionClass($className);
        $method    = $object->getMethod($methodName);

        $lastControllerRoute = $this->defaultRoute;
        foreach ($this->reader->getClassAnnotations($object) as $annotation) {
            $this->procesar($annotation, $attributes);
            if (get_class($annotation) == $this->breadcrumbAnnotation) {
                $lastControllerRoute = $annotation->route;
            }
        }

        $i = 0;
        foreach ($this->reader->getMethodAnnotations($method) as $annotation) {
            $this->procesar($annotation, $attributes, $i);
        }
        if (isset($attributes['_route'])) {
            $this->setBreadcrumb($i, $attributes['_route'] == $lastControllerRoute);
        }
	}

    private function procesar($annotation, $attributes, &$i = 0)
    {
        switch (get_class($annotation)) {
            case $this->breadcrumbAnnotation:
                $this->procesarBreadcrumb($annotation, $attributes);
                $i++;
                break;

            default:
                break;
        }
    }

    private function procesarBreadcrumb($annotation, $attributes)
    {
        $ruta = $annotation->route ?: $attributes['_route'];

        if ($annotation->titleParams) {
            $args = array();
            foreach ($annotation->titleParams as $key => $value) {
                if ($value[0] == '$') {
                    $args[] = $this->getArgument($value, $attributes);
                }
                else {
                    $args[] = $value;
                }
            }
            $titulo = vsprintf($annotation->title, $args);
        }
        else {
            $titulo = $annotation->title;
        }

        if ($annotation->params) {
            foreach ($annotation->params as $key => $value) {
                if ($value[0] == '$') {
                    $annotation->params[$key] = $this->getArgument($value, $attributes);
                }
            }
            $params = $annotation->params;
        }
        else {
            $params = $annotation->route ? array() : $attributes['_route_params'];
        }
        $path = array($titulo, $ruta, $params);

        if ($annotation->dinamic == true) {
            $this->breadcrumbAdd[] = $path;
        }
        else {
            $this->breadcrumb[] = $path;
        }
    }

    private function setBreadcrumb($breadcrumbMethodCount, $esDefault)
    {
        if ($this->breadcrumbAdd) {
            foreach ($this->breadcrumbAdd as $bc) {
                if (isset($bc[Breadcrumb::PARAM])) {
                    $this->breadcrumbService->add($bc[Breadcrumb::TITLE], $bc[Breadcrumb::ROUTE], $bc[Breadcrumb::PARAM]);
                }
                else {
                    $this->breadcrumbService->add($bc[Breadcrumb::TITLE], $bc[Breadcrumb::ROUTE]);
                }
            }
        }
        elseif ($breadcrumbMethodCount > 0 || $esDefault) {
            $this->breadcrumbService->set($this->breadcrumb);
        }
    }

	private function getArgument($argument, $attributes)
	{
		$key = substr($argument, 1);
        $keys = explode('.', $key);

        if (count($keys) == 1) {
		    return array_key_exists($key, $attributes) ? $attributes[$key] : null;
        }
        else {
            $value = $attributes[$keys[0]];
            unset($keys[0]);
            foreach ($keys as $next) {
                $method = 'get'.ucfirst($next);
                $value = $value->{$method}();
            }
            return $value;
        }
	}
}
