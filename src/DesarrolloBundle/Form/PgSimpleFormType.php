<?php

namespace DesarrolloBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PgSimpleFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('page', HiddenType::class, array(
                'label' => 'Página:'
            ))
            ->add('cantResultsPage', ChoiceType::class, array(
                'label' => 'Mostrar: ',
                'choices' => $options['cantResultsPageOptions']
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'cantResultsPageOptions' => array(
                '10' => 10,
                '20' => 20,
                '30' => 30,
                '40' => 40,
                '50' => 50
            ),
        ));

        $resolver->setDefined(array(
            'cantResultsPageOptions',
        ));
    }

    public function getBlockPrefix()
    {
        return 'desarrollobundle_paginadosimpleformtype';
    }
}
