<?php

namespace DesarrolloBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PaginadoFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('page', HiddenType::class, array(
                'label' => 'Página:'
            ))
            ->add('cantResultsPage', ChoiceType::class, array(
                'label' => 'Mostrar: ',
                'choices' => $options['cantResultsPageOptions']
            ))
            ->add('ordenarPor', ChoiceType::class, array(
                'label' => 'Orden:',
                'choices' => $options['orderByChoices']
            ))
            ->add('orden', ChoiceType::class, array(
                'choices' => array(
                    'asc'  => 'Ascendente',
                    'desc'  => 'Descendente',
                )
            ))
            ->add('massIds', HiddenType::class)
            ->add('massAll', HiddenType::class)
            ->add('limpiar', HiddenType::class)
        ;
        // Campo opcional para formulario de búsqueda
        if (isset($options['filtro']))
            $builder->add('filtros', $options['filtro']);

        if (isset($options['massactionForm']))
            $builder->add('massForm', $options['massactionForm']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'cantResultsPageOptions' => array(
                '10' => 10,
                '20' => 20,
                '30' => 30,
                '40' => 40,
                '50' => 50
            ),
        ));

        $resolver->setDefined(array(
            'filtro',
            'cantResultsPageOptions',
            'massactionForm',
        ));
        $resolver->setRequired(array(
            'orderByChoices',
        ));
    }

    public function getBlockPrefix()
    {
        return 'desarrollobundle_paginadoformtype';
    }
}
