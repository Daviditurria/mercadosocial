<?php

namespace DesarrolloBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Config\Definition\Processor;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class DesarrolloExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $processor = new Processor();
        $configuration = new Configuration();
        $config = $processor->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $paginadorServiceDefinition = $container->getDefinition('minsaludba.paginador');
        $paginadorServiceDefinition->addMethodCall('setConfig', array(
            $config['paginador']['filters_theme'],
            $config['paginador']['base_layout'],
            $config['paginador']['panel_state'],
            $config['paginador']['show_order'],
            $config['paginador']['show_rows_per_page'],
            $config['paginador']['show_rows_at_first'],
            $config['paginador']['always_show_page_nav']
        ));

        $pg_simpleServiceDefinition = $container->getDefinition('minsaludba.pg_simple');
        $pg_simpleServiceDefinition->addMethodCall('setConfig', array(
            $config['pg_simple']['base_layout'],
            $config['pg_simple']['show_rows_per_page']
        ));

        /*$breadcrumbServiceDefinition = $container->getDefinition('.breadcrumb');
        $breadcrumbServiceDefinition->addMethodCall('setConfig', array(
            $config['breadcrumb']['root_title'],
            $config['breadcrumb']['root_name'],
            $config['breadcrumb']['indice']
        ));

        $annotationListenerDefinition = $container->getDefinition('.annotation_listener');
        $annotationListenerDefinition->addMethodCall('setConfig', array(
            $config['breadcrumb']['root_name']
        ));*/
    }
}
