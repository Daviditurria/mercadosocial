<?php

namespace DesarrolloBundle\Util\Breadcrumb;

/**
* Breadcrumb
*/
class Breadcrumb
{
	const TITLE = 0;
	const ROUTE = 1;
	const PARAM = 2;

	/**
	 * @var Session
	 */
	private $session;

	/**
	 * @var string
	 */
	private $indice;

	/**
	 * @var string
	 */
	private $defaultTitle;

	/**
	 * @var Container
	 */
	private $container;

	/**
	 * @var string
	 */
	private $defaultName;

	/**
	 * @param Session $session
	 * @param Container $container
	 */
	public function __construct($session, $container)
	{
		$this->session = $session;
		$this->container = $container;
	}

	/**
	 * @param string $defaultTitle
	 * @param string $defaultName
	 * @param string $indice
	 */
	public function setConfig($defaultTitle, $defaultName, $indice)
	{
		$this->defaultTitle = $defaultTitle;
		$this->defaultName = $defaultName;
		$this->indice = 'breadcrumbs_'.$indice;
	}

	/**
	 * @param array $breadcrumb
	 * @return Breadcrumb
	 */
	public function set($breadcrumb)
	{
		$root = array($this->defaultTitle, $this->defaultName);
		array_unshift($breadcrumb, $root);
		$this->session->set($this->indice, $breadcrumb);

		return $this;
	}

	/**
	 * @param string $titulo
	 * @param string $ruta
	 * @param array|null $params
	 * @return Breadcrumb
	 */
	public function add($titulo, $ruta, $params = null)
	{
		$breadcrumb = $this->session->get($this->indice);

		if ($params) {
			$pathActual = array($titulo, $ruta, $params);
		}
		else {
			$pathActual = array($titulo, $ruta);
		}

        if (!in_array($pathActual, $breadcrumb)) {
            array_push($breadcrumb, $pathActual);
        }else {
            while(end($breadcrumb) != $pathActual){
                array_pop($breadcrumb);
            }
        }
        $this->session->set($this->indice, $breadcrumb);

        return $this;
	}
}
