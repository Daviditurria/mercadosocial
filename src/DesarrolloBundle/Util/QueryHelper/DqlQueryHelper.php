<?php

namespace DesarrolloBundle\Util\QueryHelper;

use Doctrine\ORM\Tools\Pagination\Paginator;

/**
* QueryHelper
*/
class DqlQueryHelper extends QueryHelper
{
    /**
     * @param string $repoName
     * @param string $em
     */
	public function setRepository($repoName, $em)
	{
		$this->repo = $this->doctrine->getRepository($repoName, $em);
	}

    /**
     * @param string $queryFunc
     * @param int $totalPages
     * @param int $totalRows
     * @param array $criterios
     * @return array
     */
	public function find($queryFunc, &$totalPages = 0, &$totalRows = 0, array $criterios)
	{
		$this->filtros = isset($criterios['filtros']) ? $criterios['filtros'] : array();
        $this->ordenarPor = isset($criterios['ordenarPor']) ? $criterios['ordenarPor'] : null;
        $this->direction = isset($criterios['orden']) ? $criterios['orden'] : null;

        // Cantidad de registros
        $totalRows = $this->repo->{$queryFunc}($this->repo->createQueryBuilder('a'), $this)
                ->select('COUNT(DISTINCT a)')
                ->resetDQLPart('groupBy')
                ->resetDQLPart('orderBy')
                ->getQuery()
                ->getSingleScalarResult();

        $limit = $criterios['cantResultsPage'];
        $offset = $criterios['page'] - 1;

        // Cálculo de las páginas
        $firstResult = $offset * $limit;

        // Consulta de registros
        $query = $this->repo->{$queryFunc}($this->repo->createQueryBuilder('a'), $this)
                ->setFirstResult($firstResult)
                ->setMaxResults($limit)
                /*->getQuery()*/;

        // $paginator = new Paginator($query, $fetchJoinCollection = true);

        // $totalRows = count($paginator);
        $totalPages = $totalRows == 0 ? 1 : ceil($totalRows / $limit);

        return $query->getQuery()->getResult();
        /*$entities = array();
        foreach ($paginator as $entity) {
            $entities[] = $entity;
        }
        return $entities;*/
	}

    /**
     * @param string $queryFunc
     * @param array $criterios
     * @return array
     */
	public function findAll($queryFunc, array $criterios)
	{
		$this->filtros = isset($criterios['filtros']) ? $criterios['filtros'] : array();
        $this->ordenarPor = $criterios['ordenarPor'];
        $this->direction = $criterios['orden'];

        // Consulta de registros
        $query = $this->repo->{$queryFunc}($this->repo->createQueryBuilder('a'), $this);

        return $query->getQuery()->getResult();
	}
}
