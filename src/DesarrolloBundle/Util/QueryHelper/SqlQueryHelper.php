<?php

namespace DesarrolloBundle\Util\QueryHelper;

/**
* SqlQueryHelper
*/
class SqlQueryHelper extends QueryHelper
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param string $repoName
     * @param string $em
     */
    public function setRepository($repoName, $em)
    {
        $this->repo = $this->doctrine->getRepository($repoName, $em);
        $this->em = $this->doctrine->getManager($em);
    }

    /**
     * @param string $queryFunc
     * @param int $totalPages
     * @param int $totalRows
     * @param array $criterios
     * @return array
     */
    public function find($queryFunc, &$totalPages = 0, &$totalRows = 0, array $criterios)
    {
        $this->filtros = isset($criterios['filtros']) ? $criterios['filtros'] : array();
        $this->ordenarPor = $criterios['ordenarPor'];
        $this->direction = $criterios['orden'];
        $limit = $criterios['cantResultsPage'];
        $page = $criterios['page'];

        $queryBuilder = $this->repo->{$queryFunc}(new SqlQueryBuilder(), $this)
                ->setPage($page)
                ->setMaxResults($limit);

        // Cantidad de registros
        $totalQuery = $this->em
                ->getConnection()
                ->prepare(
                    $queryBuilder->getTotalRows()
                )
        ;
        foreach ($queryBuilder->getParameters() as $param) {
            $totalQuery->bindValue($param['key'], $param['value'], $param['type']);
        }
        $totalQuery->execute();
        $totalRows = $totalQuery->fetchAll();
        $totalRows = $totalRows[0]['total'];

        // Cálculo de las páginas
        $totalPages = $totalRows == 0 ? 1 : ceil($totalRows / $limit);

        // Consulta de registros
        $query = $this->em
                ->getConnection()
                ->prepare(
                    $queryBuilder->getQuery(
                        $this->em
                        ->getConnection()
                        ->getDriver()
                        ->getName()
                    )
                )
        ;
        foreach ($queryBuilder->getParameters() as $param) {
            $query->bindValue($param['key'], $param['value'], $param['type']);
        }
        $query->execute();

        return $query->fetchAll();
    }

    /**
     * @param string $queryFunc
     * @param array $criterios
     * @return array
     */
    public function findAll($queryFunc, array $criterios)
    {
        $this->filtros = isset($criterios['filtros']) ? $criterios['filtros'] : array();
        $this->ordenarPor = $criterios['ordenarPor'];
        $this->direction = $criterios['orden'];

        $queryBuilder = $this->repo->{$queryFunc}(new SqlQueryBuilder(), $this);

        // Consulta de registros
        $query = $this->em
                ->getConnection()
                ->prepare(
                    $queryBuilder->getAllQuery()
                )
        ;
        foreach ($queryBuilder->getParameters() as $param) {
            $query->bindValue($param['key'], $param['value'], $param['type']);
        }
        $query->execute();

        return $query->fetchAll();
    }
}
