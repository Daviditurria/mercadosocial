<?php

namespace DesarrolloBundle\Util\PaginadorSimple;

interface PgSimpleInterface
{
	/**
       * @param string $repoName
       * @param string $queryFunc
       * @param string $em
       * @return Response
       */
	public function paginate($repoName, $queryFunc = 'armarQuery', $em = null);

      /**
       * @param string $repoName
       * @param string $queryFunc
       * @param string $em
       * @return Response
       */
      public function sqlPaginate($repoName, $queryFunc = 'armarQuery', $em = null);

	/**
       * @param string $html
       * @return PaginadorInterface
       */
	public function setView($html);

	/**
       * @param array $params
       * @return PaginadorInterface
       */
	public function addViewParams(array $params);

	/**
       * @param array $params
       * @return PaginadorInterface
       */
	public function addQueryParams(array $params);

	/**
       * @param int $page
       * @return PaginadorInterface
       */
	public function setPage($page);

	/**
       * @param int $rows
       * @param array $choices
       * @return PaginadorInterface
       */
	public function setRowsPerPage($rows, $choices = null);

	/**
       * @param boolean $show
       * @return PaginadorInterface
       */
	public function showRowsPerPage($show = true);

	/**
       * @param string $layout
       * @return PaginadorInterface
       */
	public function setBaseLayout($layout);
}
