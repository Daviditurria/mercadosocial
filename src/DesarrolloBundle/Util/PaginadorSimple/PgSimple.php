<?php

namespace DesarrolloBundle\Util\PaginadorSimple;

use Symfony\Component\HttpFoundation\JsonResponse;
use \DesarrolloBundle\Form\PgSimpleFormType;

/**
 * Paginador
 */
class PgSimple implements PgSimpleInterface
{
    /**
     * Id del formulario
     *
     * @var string
     */
    private $id;

    /**
     * Servicio para acceder a otros servicios
     *
     * @var Container
     */
    private $container;

    /**
     * Servicio encargado de hacer las consultas a la base de datos
     *
     * @var DqlQueryHelper
     */
    private $queryHelper;

    /**
     * Servicio encargado de hacer las consultas a la base de datos
     *
     * @var SqlQueryHelper
     */
    private $sqlQueryHelper;

    /**
     * Request actual
     *
     * @var Request
     */
    private $request;

    /**
     * Arreglo con los valores por defecto que toma el formulario
     *
     * @var array
     */
    private $defaultData;

    /**
     * Arreglo donde se setea el formulario de filtros,
     * las opciones de orden y las de cantidad de resultados por página
     *
     * @var array
     */
    private $options;

    /**
     * Arreglo que determina que componentes de la vista hay que ocultar
     *
     * @var array
     */
    private $hide;

    /**
     * Arreglo con los parámetros adicionales que se le quieren pasar a la vista
     *
     * @var array
     */
    public $params;

    /**
     * Arreglo con los parámetros adicionales que se le quieren pasar
     * a la consulta que realiza el queryHelper
     *
     * @var array
     */
    private $extraData;

    /**
     * Nombre del archivo twig del cual extiende la vista del paginador
     *
     * @var string
     */
    private $layout;

    /**
     * Nombre de la vista que se debe entregar
     *
     * @var string
     */
    private $view;

    /**
     * Valor que define si usa una consulta sql
     *
     * @var boolean
     */
    private $sql;

    /**
     * Constructor
     *
     * @param Container $container
     * @param FormRemember $formRemember
     * @param DqlQueryHelper $queryHelper
     * @param SqlQueryHelper $sqlQueryHelper
     */
    public function __construct($container, $request, $queryHelper, $sqlQueryHelper)
    {
        $this->id = uniqid("form-");
        $this->container = $container;
        $this->queryHelper = $queryHelper;
        $this->sqlQueryHelper = $sqlQueryHelper;
        $this->request = $request->getCurrentRequest();
        $this->defaultData = array(
            'page' => 1,
            'cantResultsPage' => 10,
        );
        $this->options = array();
        $this->extraData = array();
        $this->sql = false;
    }

    /**
     * Carga los valores definidos en config.yml o los por defecto
     *
     * @param string $temaFiltro
     * @param string $layout
     */
    public function setConfig($layout, $showCant)
    {
        $this->layout = $layout;
        $this->showRowsPerPage($showCant);
    }

    /**
     * Método encargado de devolver la respuesta
     * Debe ser el último en llamarse
     *
     * @param string $repoName
     * @param string $queryFunc
     * @param string $em
     * @return Response
     */
    public function paginate($repoName, $queryFunc = 'armarQuery', $em = null)
    {
        $form = $this->createForm();

        $this->setRepository($repoName, $em);

        $data = $this->getFormData($form);

        $this->agregarData($data);

        $entities = $this->getEntities($queryFunc, $totalRows, $totalPages, $data);

        return $this->createResponse($form, $entities, $totalRows, $totalPages);
    }

    /**
     * Método encargado de devolver la respuesta
     * usando una consulta sql
     * Debe ser el último en llamarse
     *
     * @param string $repoName
     * @param string $queryFunc
     * @param string $em
     * @return Response
     */
    public function sqlPaginate($repoName, $queryFunc = 'armarQuery', $em = null)
    {
        return $this->sql()->paginate($repoName, $queryFunc, $em);
    }

    /**
     * Determina que se va a usar una
     * consulta sql
     *
     * @return Paginador
     */
    private function sql()
    {
        $this->sql = true;

        return $this;
    }


    /**
     * Define el nombre de la vista que se debe entregar
     *
     * @param string $html
     * @return Paginador
     */
    public function setView($html)
    {
        $this->view = $html;

        return $this;
    }

    /**
     * Setea los parámetros adicionales que se le quieren
     * pasar a la vista
     *
     * @param array $params
     * @return Paginador
     */
    public function addViewParams(array $params)
    {
        $this->params = $params;

        return $this;
    }

    /**
     * Setea los parámetros adicionales que se le quieren
     * pasar a la consulta que realiza el queryHelper
     *
     * @param array $params
     * @return Paginador
     */
    public function addQueryParams(array $params)
    {
        $this->extraData = $params;

        return $this;
    }

    /**
     * Setea la página por defecto
     *
     * @param int $page
     * @return Paginador
     */
    public function setPage($page)
    {
        $this->defaultData['page'] = $page;

        return $this;
    }

    /**
     * Setea las cantidades de resultados que se muestran por página
     *
     * @param int $rows
     * @param array $choices
     * @return Paginador
     */
    public function setRowsPerPage($rows, $choices = null)
    {
        $this->defaultData['cantResultsPage'] = $rows;

        if ($choices) {
            $ch = array();
            foreach ($choices as $key => $value) {
                $ch[intval($value)] = $value;
            }
            $this->options['cantResultsPageOptions'] = $ch;
        }

        return $this;
    }

    /**
     * Determina si el combo de cantidades de
     * resultados se debe mostrar
     *
     * @return Paginador
     */
    public function showRowsPerPage($show = true)
    {
        $this->hide['cantPorPagina'] = !$show;

        return $this;
    }

    /**
     * @param string $layout
     * @return Paginador
     */
    public function setBaseLayout($layout)
    {
        $this->layout = $layout;

        return $this;
    }

    /**
     * @return Form
     */
    private function createForm()
    {
        return $this->container->get('form.factory')
               ->create(
                   PgSimpleFormType::class,
                   $this->defaultData,
                   $this->options
                );
    }

    /**
     * @param string $repoName
     * @param string $em
     * @return Paginador
     */
    private function setRepository($repoName, $em)
    {
        $this->getQueryHelper()->setRepository($repoName, $em);

        return $this;
    }

    /**
     * @return QueryHelper
     */
    private function getQueryHelper()
    {
        return $this->sql ? $this->sqlQueryHelper : $this->queryHelper;
    }

    /**
     * @param Form $form
     * @return array
     */
    private function getFormData(&$form)
    {
        if ($this->isAjax()) {
            $ajaxData = $this->request->request->get($form->getName());
            $form->setData($ajaxData);
        }

        return $this->submit($form);
    }

    /**
     * @param Form $form
     * @return array
     */
    private function submit(&$form)
    {
        if ($this->request->isMethod('POST')) {
            $form->handleRequest($this->request);
        }
        $data = $form->getData();

        return $data;
        // return $this->dataValida($data);
    }

    /**
     * @return boolean
     */
    private function isAjax()
    {
        return $this->request->isXmlHttpRequest();
    }

    /**
     * @return boolean
     */
    private function isGET()
    {
        return $this->request->isMethod('GET');
    }

    /**
     * @param array $data
     * @return array
     */
    private function dataValida($data)
    {
        foreach ($data as $clave => $valor) {
            if ($valor == null && $this->defaultData[$clave] != $valor) {
                return $this->defaultData;
            }
        }

        return $data;
    }

    /**
     * @param array $data
     * @return Paginador
     */
    private function agregarData(&$data)
    {
        if ($this->extraData) {
            $data['filtros'] = $this->extraData;
        }

        return $this;
    }

    /**
     * @param string $queryFunc
     * @param int $totalPages
     * @param int $totalRows
     * @param array $data
     * @return array
     */
    private function find($queryFunc, &$totalPages, &$totalRows, $data)
    {
        return $this->getQueryHelper()->find($queryFunc, $totalPages, $totalRows, $data);
    }

    /**
     * @param string $queryFunc
     * @param int $totalRows
     * @param int $totalPages
     * @param array $data
     * @param boolean $validFilters
     * @return array
     */
    private function getEntities($queryFunc, &$totalRows = 0, &$totalPages = 0, $data)
    {
        $entities = array();
        $totalRows = 0;
        $totalPages = 0;
        $entities = $this->find($queryFunc, $totalPages, $totalRows, $data);

        return $entities;
    }

    /**
     * @param Form $form
     * @param array $entities
     * @param int $totalRows
     * @param int $totalPages
     * @return Response|JsonResponse|array
     */
    private function createResponse($form, $entities, $totalRows, $totalPages)
    {
        return $this->isAjax()
                ? $this->createAjaxPage($entities, $totalRows, $totalPages)
                : $this->createPage($form, $entities, $totalRows, $totalPages);
    }

    /**
     * @param array $entities
     * @param int $totalRows
     * @param int $totalPages
     * @return JsonResponse
     */
    private function createAjaxPage($entities, $totalRows, $totalPages)
    {
        $twig = $this->container->get('twig');
        $template = $twig->loadTemplate($this->view);
        $params = array_merge(
            array(
                'paginador' => array(
                    'entities' => $entities
                )
            ), $this->params ? $this->params : array()
        );
        $html = $template->renderBlock('paginado_row', $twig->mergeGlobals($params), array(), false);

        $response = array(
            'html' => $html,
            'totalRows' => $totalRows,
            'totalPages' => $totalPages
        );

        return new JsonResponse($response);
    }

    /**
     * @param Form $form
     * @param array $entities
     * @param int $totalRows
     * @param int $totalPages
     * @return Response|array
     */
    private function createPage($form, $entities, $totalRows, $totalPages)
    {
        $templating = $this->container->get('templating');
        $paginador = array_merge(
            array(
                'paginador' => new PaginaSimple(
                    $this->id,
                    $form->createView(),
                    $entities,
                    $totalPages,
                    $totalRows,
                    $this->hide,
                    $this->layout
                )
            ), $this->params ? $this->params : array()
        );
        return $templating->renderResponse($this->view, $paginador);
    }

}
