<?php

namespace DesarrolloBundle\Util\PaginadorSimple;

/**
* Pagina
*/
class PaginaSimple
{
	/**
	 * @var string
	 */
	public $id;

	/**
	 * @var Form
	 */
	public $form;

	/**
	 * @var array
	 */
	public $entities;

	/**
	 * @var int
	 */
	public $totalPages;

	/**
	 * @var int
	 */
	public $totalRows;

	/**
	 * @var array
	 */
	public $hide;

	/**
	 * @var string
	 */
	public $layout;

	/**
	 * @param string $id
	 * @param Form $form
	 * @param array $entities
	 * @param int $totalPages
	 * @param int $totalRows
	 * @param array $exports
	 * @param array $massActions
	 * @param string $estadoPanel
	 * @param array $hide
	 * @param string $temaFiltro
	 * @param string $layout
	 */
	function __construct($id, $form, $entities, $totalPages, $totalRows, $hide, $layout)
	{
		$this->id = $id;
		$this->form = $form;
		$this->entities = $entities;
		$this->totalPages = $totalPages;
		$this->totalRows = $totalRows;
		$this->hide = $hide;
		$this->layout = $layout;
	}
}
