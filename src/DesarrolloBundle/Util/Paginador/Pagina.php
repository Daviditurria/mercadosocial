<?php

namespace DesarrolloBundle\Util\Paginador;

/**
* Pagina
*/
class Pagina
{
	/**
	 * @var string
	 */
	public $id;

	/**
	 * @var Form
	 */
	public $form;

	/**
	 * @var array
	 */
	public $entities;

	/**
	 * @var int
	 */
	public $totalPages;

	/**
	 * @var int
	 */
	public $totalRows;

	/**
	 * @var Export[]
	 */
	public $exports;

	/**
	 * @var MassAction[]
	 */
	public $massActions;

	/**
	 * @var string
	 */
	public $estadoPanel;

	/**
	 * @var array
	 */
	public $hide;

	/**
	 * @var string
	 */
	public $temaFiltro;

	/**
	 * @var string
	 */
	public $layout;

	/**
	 * @param string $id
	 * @param Form $form
	 * @param array $entities
	 * @param int $totalPages
	 * @param int $totalRows
	 * @param array $exports
	 * @param array $massActions
	 * @param string $estadoPanel
	 * @param array $hide
	 * @param string $temaFiltro
	 * @param string $layout
	 */
	function __construct($id, $form, $entities, $totalPages, $totalRows, $exports, $massActions, $estadoPanel, $hide, $temaFiltro, $layout)
	{
		$this->id = $id;
		$this->form = $form;
		$this->entities = $entities;
		$this->totalPages = $totalPages;
		$this->totalRows = $totalRows;
		$this->exports = $exports;
		$this->massActions = $massActions;
		$this->estadoPanel = $estadoPanel;
		$this->hide = $hide;
		$this->temaFiltro = $temaFiltro;
		$this->layout = $layout;
	}
}
