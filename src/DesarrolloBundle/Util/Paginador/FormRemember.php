<?php

namespace DesarrolloBundle\Util\Paginador;

/**
 * FormRemember
 *
 */
class FormRemember
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var Request
     */
    private $request;

    /**
     * @param Session $session
     * @param RequestStack $request
     */
    public function __construct($session, $request)
    {
        $this->session = $session;
        $this->request = $request->getCurrentRequest();
    }

    /**
     * @param Form $form
     * @param boolean $remember
     */
    public function remember(&$form, $remember)
    {
        $indice = 'form_remember_' . $this->getName();
        // Si la petición es POST, inicializo las variables de sesión.
        // Sino, enlazo el formulario con lo que se encuentre en sesión.
        if ($this->request->isMethod('POST')) {
            $form->handleRequest($this->request);
            $data = $form->getData();
            if ($data["limpiar"] === true || $data["limpiar"] === "true") {
                $this->session->remove($indice);
                return true;
            }
            $this->session->set($indice, $this->request);
        }
        elseif ($remember && $this->session->has($indice)) {
            $form->handleRequest($this->session->get($indice));
        }

        return false;

    }

    /**
     * @param Form $form
     */
    public function remove(&$form)
    {
        $indice = 'form_remember_' . $this->getName();

        $this->session->remove($indice);
    }

    /**
     * @return string
     */
    private function getName()
    {
        $uri = $this->request->getPathInfo();
        return $uri;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

}
