<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EntidadDeCaridad
 *
 * @ORM\Table(name="entidadDeCaridad")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EntidadDeCaridadRepository")
 */
class EntidadDeCaridad
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, unique=true)
     */
    private $nombre;

    /**
     * @ORM\Column(name="dineroRecaudado", type="integer")
     */
    private $dineroRecaudado;

    public function __toString(){
        return $this->getNombre();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return EntidadDeCaridad
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set dineroRecaudado
     *
     * @param integer $dineroRecaudado
     *
     * @return EntidadDeCaridad
     */
    public function setDineroRecaudado($dineroRecaudado)
    {
        $this->dineroRecaudado = $dineroRecaudado;

        return $this;
    }

    /**
     * Get dineroRecaudado
     *
     * @return integer
     */
    public function getDineroRecaudado()
    {
        return $this->dineroRecaudado;
    }
}
