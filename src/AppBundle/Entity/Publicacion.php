<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Publicacion
 *
 * @ORM\Table(name="publicacion")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PublicacionRepository")
 */
class Publicacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;    

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=255)
     */
    private $estado;

    /**
     * @var integer
     *
     * @ORM\Column(name="precio", type="integer", length=10)
     */
    private $precio;

    /**
     * @var integer
     *
     * @ORM\Column(name="precioMaximo", type="integer", length=10)
     */
    private $precioMaximo;

    /**
     * @var integer
     *
     * @ORM\Column(name="precioMinimo", type="integer", length=10)
     */
    private $precioMinimo;

    /**
     * @var integer
     *
     * @ORM\Column(name="precioActual", type="integer", length=1)
     */
    private $precioActual;

    /**
     * @var integer
     *
     * @ORM\Column(name="stock", type="integer", length=10)
     */
    private $stock;

    /**
     * @var integer
     *
     * @ORM\Column(name="cantidadVendida", type="integer", length=10)
     */
    private $cantidadVendida;

    /**
     * @ORM\OneToMany(targetEntity="Comentario", mappedBy="publicacion", cascade={"remove"})
     */
    private $comentarios;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date")
     */
    private $fecha;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="publicaciones")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    private $usuario;

    /**
     * @ORM\ManyToOne(targetEntity="Producto", inversedBy="publicaciones")
     * @ORM\JoinColumn(name="producto_id", referencedColumnName="id")
     */
    private $producto;

    /**
     * @ORM\Column(name="ruta_foto", type="string", length=255, nullable=true)
     */
    private $rutaFoto;

    
    private $foto;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     *
     * @return self
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * @return integer
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * @param integer $precio
     *
     * @return self
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * @return integer
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param integer $stock
     *
     * @return self
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * @return integer
     */
    public function getPrecioMaximo()
    {
        return $this->precioMaximo;
    }

    /**
     * @param integer $precioMaximo
     *
     * @return self
     */
    public function setPrecioMaximo($precioMaximo)
    {
        $this->precioMaximo = $precioMaximo;

        return $this;
    }

    /**
     * @return integer
     */
    public function getPrecioMinimo()
    {
        return $this->precioMinimo;
    }

    /**
     * @param integer $precioMinimo
     *
     * @return self
     */
    public function setPrecioMinimo($precioMinimo)
    {
        $this->precioMinimo = $precioMinimo;

        return $this;
    }

    /**
     * @return integer
     */
    public function getPrecioActual()
    {
        return $this->precioActual;
    }

    /**
     * @param integer $precioActual
     *
     * @return self
     */
    public function setPrecioActual($precioActual)
    {
        $this->precioActual = $precioActual;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getComentarios()
    {
        return $this->comentarios;
    }

    /**
     * @param mixed $comentarios
     *
     * @return self
     */
    public function setComentarios($comentarios)
    {
        $this->comentarios = $comentarios;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param mixed $usuario
     *
     * @return self
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->comentarios = new \Doctrine\Common\Collections\ArrayCollection();
        $this->estado = 'activo';
        $this->cantidadVendida = 0;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Publicacion
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Add comentarios
     *
     * @param \AppBundle\Entity\Comentario $comentarios
     * @return Publicacion
     */
    public function addComentario(\AppBundle\Entity\Comentario $comentarios)
    {
        $this->comentarios[] = $comentarios;

        return $this;
    }

    /**
     * Remove comentarios
     *
     * @param \AppBundle\Entity\Comentario $comentarios
     */
    public function removeComentario(\AppBundle\Entity\Comentario $comentarios)
    {
        $this->comentarios->removeElement($comentarios);
    }

    /**
     * Set rutaFoto
     *
     * @param string $rutaFoto
     *
     * @return Publicacion
     */
    public function setRutaFoto($rutaFoto)
    {
        $this->rutaFoto = $rutaFoto;

        return $this;
    }

    /**
     * Get rutaFoto
     *
     * @return string
     */
    public function getRutaFoto()
    {
        return $this->rutaFoto;
    }

    /**
     * Set foto.
     *
     * @param UploadedFile $foto
     */
    public function setFoto(UploadedFile $foto = null)
    {
        $this->foto = $foto;
    }
    /**
     * Get foto.
     *
     * @return UploadedFile
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * Uploads the profile photo
     *
     * @param string $dir
     */
    public function uploadFoto($dir)
    {
        if (null === $this->getFoto()) {
            return;
        }
        $photoFileName = uniqid('publicacion').'.'.$this->getFoto()->guessExtension();
        $this->getFoto()->move($dir, $photoFileName);
        $this->setRutaFoto($photoFileName);
    }


    /**
     * Set producto
     *
     * @param \AppBundle\Entity\Producto $producto
     *
     * @return Publicacion
     */
    public function setProducto(\AppBundle\Entity\Producto $producto = null)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return \AppBundle\Entity\Producto
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Publicacion
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set estado
     *
     * @param string $estado
     *
     * @return Publicacion
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    public function getPrecioEstandar(){
        return $this->getPrecio();
    }

    /**
     * Set cantidadVendida
     *
     * @param integer $cantidadVendida
     *
     * @return Publicacion
     */
    public function setCantidadVendida($cantidadVendida)
    {
        $this->cantidadVendida = $cantidadVendida;

        return $this;
    }

    /**
     * Get cantidadVendida
     *
     * @return integer
     */
    public function getCantidadVendida()
    {
        return $this->cantidadVendida;
    }
}
