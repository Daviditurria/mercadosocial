<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Carrito
 *
 * @ORM\Table(name="carrito")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CarritoRepository")
 */
class Carrito
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\OneToOne(targetEntity="Usuario", inversedBy="carrito")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    private $usuario;

    /**
     * @ORM\OneToMany(targetEntity="ProductoEnCarrito", mappedBy="carrito", cascade={"remove", "persist"}, orphanRemoval=true)
     */
    private $productoEnCarrito;

    /**
     * Constructor
     */
    public function __construct($usuario)
    {
        $this->productoEnCarrito = new ArrayCollection();
        $this->usuario = $usuario;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param mixed $usuario
     *
     * @return self
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Add productoEnCarrito
     *
     * @param \AppBundle\Entity\ProductoEnCarrito $productoEnCarrito
     *
     * @return Carrito
     */
    public function addProductoEnCarrito(\AppBundle\Entity\ProductoEnCarrito $productoEnCarrito)
    {
        $this->productoEnCarrito[] = $productoEnCarrito;

        return $this;
    }

    /**
     * Remove productoEnCarrito
     *
     * @param \AppBundle\Entity\ProductoEnCarrito $productoEnCarrito
     */
    public function removeProductoEnCarrito(\AppBundle\Entity\ProductoEnCarrito $productoEnCarrito)
    {
        $productoEnCarrito->setCarrito(null);
        $this->productoEnCarrito->removeElement($productoEnCarrito);
    }

    /**
     * Get productoEnCarrito
     */
    public function getProductoEnCarrito()
    {
        return $this->productoEnCarrito;
    }

    public function getMontoTotal(){
        $montoTotal = 0;
        foreach ($this->getProductoEnCarrito() as $productoEnCarrito) {
            $montoTotal = $montoTotal + $productoEnCarrito->getPublicacion()->getPrecioActual();
        }
        return $montoTotal;
    }
}
