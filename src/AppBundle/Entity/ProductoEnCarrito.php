<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Publicacion
 *
 * @ORM\Table(name="productoEnCarrito")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductoEnCarritoRepository")
 */
class ProductoEnCarrito
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Publicacion")
     * @ORM\JoinColumn(name="publicacion_id", referencedColumnName="id")     
     */
    private $publicacion;

    /**
     * @var integer
     *
     * @ORM\Column(name="cantidad", type="integer", length=10)
     */
    private $cantidad;

    /**
     * @ORM\ManyToOne(targetEntity="Carrito", inversedBy="productoEnCarrito")
     * @ORM\JoinColumn(name="carrito_id", referencedColumnName="id")
     */
    private $carrito;


    public function __construct($publicacion, $cantidad, $carrito){
        $this->setPublicacion($publicacion);
        $this->cantidad = $cantidad;
        $this->carrito = $carrito;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return integer
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * @param integer $cantidad
     *
     * @return self
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCarrito()
    {
        return $this->carrito;
    }

    /**
     * @param mixed $carrito
     *
     * @return self
     */
    public function setCarrito($carrito)
    {
        $this->carrito = $carrito;

        return $this;
    }

    /**
     * Set publicacion
     *
     * @param \AppBundle\Entity\Publicacion $publicacion
     *
     * @return ProductoEnCarrito
     */
    public function setPublicacion(\AppBundle\Entity\Publicacion $publicacion = null)
    {
        $this->publicacion = $publicacion;

        return $this;
    }

    /**
     * Get publicacion
     *
     * @return \AppBundle\Entity\Publicacion
     */
    public function getPublicacion()
    {
        return $this->publicacion;
    }
}
