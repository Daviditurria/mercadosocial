<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Movimiento
 *
 * @ORM\Table(name="movimiento")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MovimientoRepository")
 */
class Movimiento
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var int
     *
     * @ORM\Column(name="monto", type="integer")
     */
    private $monto;

    /**
     * @ORM\ManyToOne(targetEntity="Billetera", inversedBy="movimientos", cascade={"persist"})
     * @ORM\JoinColumn(name="billetera_id", referencedColumnName="id")
     */
    private $billetera;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string")
     */
    private $tipo;

    public function __construct($billetera, $monto, $tipo){
        $this->billetera = $billetera;
        $this->fecha = new \DateTime('now');
        $this->monto = $monto;
        $this->tipo = $tipo;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Movimiento
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set monto
     *
     * @param integer $monto
     *
     * @return Movimiento
     */
    public function setMonto($monto)
    {
        $this->monto = $monto;

        return $this;
    }

    /**
     * Get monto
     *
     * @return int
     */
    public function getMonto()
    {
        return $this->monto;
    }

    /**
     * Set billetera
     *
     * @param \AppBundle\Entity\Billetera $billetera
     *
     * @return Movimiento
     */
    public function setBilletera(\AppBundle\Entity\Billetera $billetera = null)
    {
        $this->billetera = $billetera;

        return $this;
    }

    /**
     * Get billetera
     *
     * @return \AppBundle\Entity\Billetera
     */
    public function getBilletera()
    {
        return $this->billetera;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Movimiento
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }
}
