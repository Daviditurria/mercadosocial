<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Carrito;
use AppBundle\Entity\Compra;
use AppBundle\Entity\Movimiento;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Carrito controller.
 *
 * @Route("carrito")
 */
class CarritoController extends Controller
{
    /**
     * Lists all carrito entities.
     *
     * @Route("/", name="carrito_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $carritos = $em->getRepository('AppBundle:Carrito')->findAll();

        return $this->render('carrito/index.html.twig', array(
            'carritos' => $carritos,
        ));
    }

    /**
     * Finds and displays a carrito entity.
     *
     * @Route("/{id}", name="carrito_show")
     * @Method("GET")
     */
    public function showAction(Carrito $carrito)
    {
        return $this->render('carrito/show.html.twig', array(
            'carrito' => $carrito,
        ));
    }

    /**
     *
     * @Route("/eliminarProducto/{idCarrito}/{idProductoEnCarrito}", name="carrito_eliminar_publicacion")
     */
    public function eliminarPublicacionAction($idCarrito, $idProductoEnCarrito){
        $em = $this->getDoctrine()->getManager();
        $carrito = $em->getRepository('AppBundle:Carrito')->find($idCarrito);
        $productoEnCarrito = $em->getRepository('AppBundle:productoEnCarrito')->find($idProductoEnCarrito);
        $productoEnCarrito->setCarrito(null);
        $carrito->removeProductoEnCarrito($productoEnCarrito);

        $publicacion=$productoEnCarrito->getPublicacion();
        $restar = $publicacion->getStock() + $productoEnCarrito->getCantidad();
        $publicacion->setStock($restar);
        
        $em->remove($productoEnCarrito);
        $em->persist($carrito);
        $em->flush();

        return $this->redirectToRoute('carrito_show', array(
            'id' => $carrito->getId(),
        ));
    }

    /**
     * @Route("/agregarCantidad/{idCarrito}/{idProductoEnCarrito}", name="carrito_agregar_cantidad")
     */
    public function agregarCantidadAction($idCarrito, $idProductoEnCarrito){
        $em = $this->getDoctrine()->getManager();
        $carrito = $em->getRepository('AppBundle:Carrito')->find($idCarrito);
        $productoEnCarrito = $em->getRepository('AppBundle:productoEnCarrito')->find($idProductoEnCarrito);
        $publicacion=$productoEnCarrito->getPublicacion();
        if ($publicacion->getStock() >= 1){
            $cantidadTotal = $productoEnCarrito->getCantidad() + 1;
            $productoEnCarrito->setCantidad($cantidadTotal);
            $em->persist($productoEnCarrito);
            $em->flush();}

        return $this->redirectToRoute('carrito_show', array(
            'id' => $carrito->getId(),
        ));
    }

    /**
     * @Route("/restarCantidad/{idCarrito}/{idProductoEnCarrito}", name="carrito_restar_cantidad")
     */
    public function restarCantidadAction($idCarrito, $idProductoEnCarrito){
        $em = $this->getDoctrine()->getManager();
        $carrito = $em->getRepository('AppBundle:Carrito')->find($idCarrito);
        $productoEnCarrito = $em->getRepository('AppBundle:productoEnCarrito')->find($idProductoEnCarrito);
        $cantidadTotal = $productoEnCarrito->getCantidad() - 1;
        $productoEnCarrito->setCantidad($cantidadTotal);
        $em->persist($productoEnCarrito);
        $em->flush();

        return $this->redirectToRoute('carrito_show', array(
            'id' => $carrito->getId(),
        ));
    }

    /**
     * @Route("/comprar/{id}", name="carrito_comprar")
     */
    public function comprarAction($id){
        $em = $this->getDoctrine()->getManager();
        $carrito = $em->getRepository('AppBundle:Carrito')->find($id);        
        $billeteraComprador = $this->getUser()->getBilletera();
        $montoTotal = $carrito->getMontoTotal();
        
        if($montoTotal > $billeteraComprador->getDinero()){
            $this->get('minsaludba.avisos')->addError('No posee dinero suficiente en su billetera para realizar esta compra ');
            return $this->redirectToRoute('publicacion_index');
        }
        foreach ($carrito->getProductoEnCarrito() as $productoEnCarrito) {
            if($productoEnCarrito->getPublicacion()->getEstado() != 'activa' || $productoEnCarrito->getPublicacion()->getStock() <= 0){
                $this->get('minsaludba.avisos')->addError('No puedes realizar compras de publicaciones deshabilitadas o sin stock, elimínalas!');

                return $this->redirectToRoute('carrito_show', array(
                    'id' => $carrito->getId()
                ));
            }
            if($productoEnCarrito->getPublicacion()->getStock() > $productoEnCarrito->getCantidad()){
                $productoEnCarrito->getPublicacion()->setStock(($productoEnCarrito->getPublicacion()->getStock() - $productoEnCarrito->getCantidad()));
                $cantidad = $productoEnCarrito->getCantidad();
                $precio = $productoEnCarrito->getPublicacion()->getPrecioActual();
                $billeteraVendedor = $productoEnCarrito->getPublicacion()->getUsuario()->getBilletera();
                
                $productoEnCarrito->getPublicacion()->setCantidadVendida($productoEnCarrito->getPublicacion()->getCantidadVendida() + $cantidad);
                $compra = new Compra($productoEnCarrito->getPublicacion(), $productoEnCarrito->getCantidad(), $this->getUser());

                $montoComprador = $cantidad * $precio * -1;
                $movimientoComprador = new Movimiento($billeteraComprador, $montoComprador, 'Compra');
                $billeteraComprador->addMovimiento($movimientoComprador);
                
                $montoVendedor = $cantidad * $precio;
                $movimientoVendedor = new Movimiento($billeteraVendedor, $montoVendedor, 'Venta');
                $billeteraVendedor->addMovimiento($movimientoVendedor);
                
                $productoEnCarrito->setCarrito(null);

                $puntosAgregar=$compra->getPrecio()/100;
                $puntosViejos=$billeteraComprador->getPuntos();
                $billeteraComprador->setPuntos($puntosViejos+$puntosAgregar);

                $em->persist($movimientoComprador);
                $em->persist($billeteraComprador);
                $em->persist($movimientoVendedor);
                $em->persist($billeteraVendedor);
                $em->persist($compra);
                $em->persist($productoEnCarrito);
            }
        }

        $em->persist($carrito);
        $em->flush();

        $this->get('minsaludba.avisos')->addSuccess('La compra ha sido efectuada correctamente.');
        
        return $this->redirectToRoute('publicacion_index');
    }
}
