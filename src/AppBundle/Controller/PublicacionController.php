<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Publicacion;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\ProductoEnCarrito;

/**
 * Publicacion controller.
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PublicacionRepository")
 * @Route("publicacion")
 */
class PublicacionController extends Controller
{
    /**
     * Lists all publicacion entities.
     *
     * @Route("/", name="publicacion_index")
     * @Route("/usuario/{idUsuario}", defaults={"idUsuario" = null}, name="publicacion_index_user")
     */
    public function indexAction($idUsuario = null)
    {
        if($idUsuario){
            $this->get('minsaludba.paginador')->closePanel();
        }
        return $this->get('minsaludba.paginador')
            ->addQueryParams(array(
                'usuario' => $this->getUser()
            ))
            ->setFilter('AppBundle\Form\FiltroPublicacionType')
            ->setRowsPerPage(10, array(
                10 => 10,
                20 => 20,
                30 => 30,
            ))
            ->noRemember()
            ->showRowsPerPage(false)
            ->showRowsAtFirst(true)
            ->setOrder(array(
                'nombre' => 'nombre',
                'cantidad' => 'cantidad',
                'precio' => 'precio',
                'stock' => 'stock'
                ),
                'cantidad',
                'desc'
            )
            ->addQueryParams(array(
                'idUsuario' => $idUsuario
            ))
            ->addViewParams(array(
                'idUsuario' => $idUsuario
            ))
            ->setView("publicacion/index.html.twig")
            ->paginate('AppBundle:Publicacion');
    }

    /**
     * Creates a new publicacion entity.
     *
     * @Route("/new", name="publicacion_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $publicacion = new Publicacion();
        $form = $this->createForm('AppBundle\Form\PublicacionType', $publicacion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $publicacion->uploadFoto($this->container->getParameter('dir.publicacion.fotos'));
            $usuario = $this->getUser();
            $publicacion->setUsuario($usuario);
            $publicacion->setFecha(new \Datetime('now'));
            $em->persist($publicacion);
            $em->flush();

            return $this->redirectToRoute('publicacion_show', array('id' => $publicacion->getId()));
        }

        return $this->render('publicacion/new.html.twig', array(
            'publicacion' => $publicacion,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a publicacion entity.
     *
     * @Route("/{id}", name="publicacion_show")
     */
    public function showAction(Request $request, Publicacion $publicacion)
    {
        $em = $this->getDoctrine()->getManager();
        $deleteForm = $this->createDeleteForm($publicacion);
        $form = $this->createForm('AppBundle\Form\SeleccionarCantidadType');
        $form->handleRequest($request);
        $comentarios = $em->getRepository('AppBundle:Comentario')->findByPublicacion($publicacion);

        $palabras = array();
        foreach ($comentarios as $key => $comentario) {
            $descripcion = $comentario->getDescripcion();
            $descripcion = str_replace(',', '', $descripcion);
            $descripcion = str_replace('.', '', $descripcion);
            $descripcion = str_replace('\n', '', $descripcion);
            $a = explode(' ', $descripcion);
            $palabras = array_merge($palabras, $a);
        }
        $nube = array_count_values($palabras);
        arsort($nube);
        $nube = array_slice($nube, 0, 10);
        $colores = array();
        array_push($colores, '#007bff');
        array_push($colores, '#28a745');
        array_push($colores, '#dc3545');
        array_push($colores, '#17a2b8');
        array_push($colores, '#6c757d');
        array_push($colores, '#E91E63');
        array_push($colores, '#9C27B0');
        array_push($colores, '#2196F3');
        array_push($colores, '#00BCD4');
        array_push($colores, '#CDDC39');


        if($form->isSubmitted() && $form->isValid()){
            $usuario = $this->getUser();
            $carrito = $usuario->getCarrito();
            $cantidad = $form->get('cantidad')->getData();

            $productosEnCarrito = $carrito->getProductoEnCarrito();
            $publicacionesEnElCarrito = array();
            foreach ($productosEnCarrito as $value) {
                array_push($publicacionesEnElCarrito, $value->getPublicacion());
            }
            if(in_array($publicacion, $publicacionesEnElCarrito)){
                $this->get('minsaludba.avisos')->addError('La publicación ya se encuentra en el carrito, agregue más cantidad solicitada!.');

                return $this->redirectToRoute('publicacion_index');
            }

            if($publicacion->getStock() >= $cantidad){
                $productoEnCarrito = new ProductoEnCarrito($publicacion, $cantidad, $carrito);
                $carrito->setUsuario($usuario);
                $carrito->addProductoEnCarrito($productoEnCarrito);

                $em->persist($carrito);
                $em->persist($productoEnCarrito);
                $em->flush();
                
                $this->get('minsaludba.avisos')->addSuccess('El producto se agregó al carrito!');
            } 
            else{
                $this->get('minsaludba.avisos')->addError('No se pudo agregar el producto al carrito. No hay suficiente stock!');
            }

            return $this->redirectToRoute('publicacion_index');
        }

        return $this->render('publicacion/show.html.twig', array(
            'publicacion' => $publicacion,
            'delete_form' => $deleteForm->createView(),
            'form' => $form->createView(),
            'nube' => $nube,
            'colores' => $colores
        ));
    }

    /**
     * Displays a form to edit an existing publicacion entity.
     *
     * @Route("/{id}/edit", name="publicacion_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Publicacion $publicacion)
    {
        $deleteForm = $this->createDeleteForm($publicacion);
        $editForm = $this->createForm('AppBundle\Form\PublicacionType', $publicacion);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            if($editForm->get("foto")->getData() != null){
                $publicacion->uploadFoto($this->container->getParameter('dir.publicacion.fotos'));
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('publicacion_show', array('id' => $publicacion->getId()));
        }

        return $this->render('publicacion/edit.html.twig', array(
            'publicacion' => $publicacion,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a publicacion entity.
     *
     * @Route("/{id}", name="publicacion_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Publicacion $publicacion)
    {
        $form = $this->createDeleteForm($publicacion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($publicacion);
            $em->flush();
        }

        return $this->redirectToRoute('publicacion_index');
    }

    /**
     * Creates a form to delete a publicacion entity.
     *
     * @param Publicacion $publicacion The publicacion entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Publicacion $publicacion)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('publicacion_delete', array('id' => $publicacion->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * @Route("/cambiarPrecio/{tipoPrecio}", name="publicacion_cambiarPrecio")
     */
    public function cambiarPrecioAction($tipoPrecio){
        $em = $this->getDoctrine()->getManager();
        $publicaciones =$em->getRepository('AppBundle:Publicacion')->findByUsuario($this->getUser());

        foreach ($publicaciones as $publicacion) {
            $publicacion->setPrecioActual($publicacion->{'getPrecio' . ucfirst($tipoPrecio)}() );
            $em->persist($publicacion);
        }

        $em->flush();

        return $this->redirectToRoute('publicacion_index_user', array('idUsuario' => $this->getUser()->getId()));
    }

    /**
     * @Route("/cambiarPorcentual/a", name="publicacion_cambiarPrecioPorcentual")
     */
    public function cambiarPrecioPorcentualAction(Request $request){
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm('AppBundle\Form\SeleccionarCantidadType');
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $publicaciones =$em->getRepository('AppBundle:Publicacion')->findByUsuario($this->getUser());
            $cantidad = $form->get('cantidad')->getData();
            foreach ($publicaciones as $publicacion) {
                $cantidad = $cantidad + 100;
                $actualizar = ($publicacion->getPrecioActual() * $cantidad / 100);
                $publicacion->setPrecioActual($actualizar);
                $em->persist($publicacion);
            }
            $em->flush();

            $this->get('minsaludba.avisos')->addSuccess('Se han actualizado tus precios!');
            return $this->redirectToRoute('publicacion_index_user', array('idUsuario' => $this->getUser()->getId()));
        }

        return $this->render('publicacion/porcentual.html.twig', array(
            'idUsuario' => $this->getUser()->getId(),
            'form' => $form->createView()
        ));
    }

}
