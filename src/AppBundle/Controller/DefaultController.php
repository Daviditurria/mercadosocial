<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\Configuracion;

class DefaultController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request)
    {
        $session = $this->getRequest()->getSession();
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        $em = $this->getDoctrine()->getManager();
        $configuracionCanje = $em->getRepository('AppBundle:Configuracion')->find(1);
        $configuracionDonacion = $em->getRepository('AppBundle:Configuracion')->find(2);
        if($configuracionCanje == null){
            $configuracionCanje = new Configuracion('100', '1');
            $em->persist($configuracionCanje);
        }
        if($configuracionCanje == null){
            $configuracionCanje = new Configuracion('10', '1');
            $em->persist($configuracionCanje);
        }
        $em->flush();

        if($error){
            $session->getFlashBag()->add('aviso_error', 'Se produjo un error. Verifique que su usuario y contraseña sea correcta.');
        }
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render(
            'default/login.html.twig',
            array(
                'last_username' => $lastUsername,
                'error'         => $error,
            )
        );
    }

// * @Security("has_role('ROLE_ADMIN')")
    /**
     * @Route("/", name="homepage")
     *
     */
    public function homepageAction()
    {
        return $this->redirectToRoute('publicacion_index');
        //return $this->render('homepage.html.twig');
    }
}