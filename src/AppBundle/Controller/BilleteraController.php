<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Billetera;
use AppBundle\Entity\Movimiento;
use AppBundle\Entity\EntidadDeCaridad;
use AppBundle\Form\DonacionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Billetera controller.
 *
 * @Route("billetera")
 */
class BilleteraController extends Controller
{
    /**
     * Lists all billetera entities.
     *
     * @Route("/", name="billetera_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $billeteras = $em->getRepository('AppBundle:Billetera')->findAll();

        return $this->render('billetera/index.html.twig', array(
            'billeteras' => $billeteras,
        ));
    }

    /**
     * Creates a new billetera entity.
     *
     * @Route("/new", name="billetera_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $billetera = new Billetera();
        $form = $this->createForm('AppBundle\Form\BilleteraType', $billetera);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($billetera);
            $em->flush();

            return $this->redirectToRoute('billetera_show', array('id' => $billetera->getId()));
        }

        return $this->render('billetera/new.html.twig', array(
            'billetera' => $billetera,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a billetera entity.
     *
     * @Route("/show", name="billetera_show")
     */
    public function showAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $billetera = $em->getRepository('AppBundle:Billetera')->find($this->getUser()->getBilletera()->getId());

        $form = $this->createForm('AppBundle\Form\SeleccionarCantidadType');
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $cantidad = $form->get('cantidad')->getData();
            $billetera->setDinero($billetera->getDinero() + $cantidad);
            $movimiento = new Movimiento($billetera, $cantidad, 'Carga');
            $em->persist($billetera);
            $em->persist($movimiento);
            $em->flush();
                
            $this->get('minsaludba.avisos')->addSuccess('Se agregó tu dinero correctamente a la Billetera!');
        }             

        return $this->render('billetera/show.html.twig', array(
            'billetera' => $billetera,
            'form' =>$form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing billetera entity.
     *
     * @Route("/{id}/edit", name="billetera_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Billetera $billetera)
    {
        $deleteForm = $this->createDeleteForm($billetera);
        $editForm = $this->createForm('AppBundle\Form\BilleteraType', $billetera);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('billetera_edit', array('id' => $billetera->getId()));
        }

        return $this->render('billetera/edit.html.twig', array(
            'billetera' => $billetera,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a billetera entity.
     *
     * @Route("/{id}", name="billetera_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Billetera $billetera)
    {
        $form = $this->createDeleteForm($billetera);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($billetera);
            $em->flush();
        }

        return $this->redirectToRoute('billetera_index');
    }

    /**
     * Creates a form to delete a billetera entity.
     *
     * @param Billetera $billetera The billetera entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Billetera $billetera)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('billetera_delete', array('id' => $billetera->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * @Route("/extraer", name="billetera_extraer")
     */
    public function extraerAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $billetera = $em->getRepository('AppBundle:Billetera')->find($this->getUser()->getBilletera()->getId());

        $form = $this->createForm('AppBundle\Form\SeleccionarCantidadType');
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $cantidad = $form->get('cantidad')->getData();
            if($billetera->getDinero() > $cantidad){
                $billetera->setDinero($billetera->getDinero() - $cantidad);
                $movimiento = new Movimiento($billetera, $cantidad * -1, 'Extracción');
                $em->persist($billetera);
                $em->persist($movimiento);
                $em->flush();
                $this->get('minsaludba.avisos')->addSuccess('Se extrajo tu dinero correctamente a la billetera!');
            }
            else{
                $this->get('minsaludba.avisos')->addError('No posees suficiente dinero en la billetera!');
            }
        }

        return $this->render('billetera/extraer.html.twig', array(
            'billetera' => $billetera,
            'form' =>$form->createView()
        ));
    }

    /**
     * @Route("/canjear", name="billetera_canjear")
     */
    public function canjearAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $billetera = $em->getRepository('AppBundle:Billetera')->find($this->getUser()->getBilletera()->getId());

        $form = $this->createForm('AppBundle\Form\SeleccionarCantidadType');
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $cantidad = $form->get('cantidad')->getData();
            $configuracion = $em->getRepository('AppBundle:Configuracion')->find(1);
            $puntosNecesarios = $configuracion->getClave();
            $dineroPorPuntos = $configuracion->getValor();
            if($cantidad < $puntosNecesarios){
                $this->get('minsaludba.avisos')->addError('Por esa cantidad de puntos (' . $cantidad . ') no puedes canjear nada!' );
            }
            elseif($billetera->getPuntos() > $cantidad){
                $monto = $dineroPorPuntos * ($cantidad / $puntosNecesarios);
                $billetera->setDinero($billetera->getDinero() + $monto);
                $billetera->setPuntos($billetera->getPuntos() - $cantidad);
                $movimiento = new Movimiento($billetera, $monto , 'Canje de puntos');
                $em->persist($billetera);
                $em->persist($movimiento);
                $em->flush();
                $this->get('minsaludba.avisos')->addSuccess('Se canjeo tu dinero correctamente !');
            }
            else{
                $this->get('minsaludba.avisos')->addError('No posees suficientes puntos para realizar el canje!');
            }
        }
        $configuracion = $em->getRepository('AppBundle:Configuracion')->find(1);
        $puntosNecesarios = $configuracion->getClave();
        $dineroPorPuntos = $configuracion->getValor();
        return $this->render('billetera/canjear.html.twig', array(
            'billetera' => $billetera,
            'form' =>$form->createView(),
            'dineroPorPuntos' => $dineroPorPuntos,
            'puntosNecesarios' => $puntosNecesarios
        ));
    }

        /**
     * @Route("/donar", name="billetera_donar")
     */
    public function donarAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $billetera = $em->getRepository('AppBundle:Billetera')->find($this->getUser()->getBilletera()->getId());

        $form = $this->createForm('AppBundle\Form\DonacionType');
        $form->handleRequest($request);
        $configuracion = $em->getRepository('AppBundle:Configuracion')->find(2);
        $puntosNecesarios = $configuracion->getClave();
        $dineroPorPuntos = $configuracion->getValor();

        if($form->isSubmitted() && $form->isValid()){
            $cantidad = $form->get('cantidad')->getData();
            if($cantidad < $puntosNecesarios){
                $this->get('minsaludba.avisos')->addError('Por esa cantidad de puntos (' . $cantidad . ') no puedes donar nada!' );
            }
            elseif($billetera->getPuntos() > $cantidad){
                $monto = $dineroPorPuntos * ($cantidad / $puntosNecesarios);
                $idEntidadDeCaridad = $form->get('entidad')->getData()->getId();
                $entidadDeCaridad = $em->getRepository('AppBundle:EntidadDeCaridad')->find($idEntidadDeCaridad);
                $entidadDeCaridad->setDineroRecaudado($entidadDeCaridad->getDineroRecaudado() + $monto);
                $billetera->setPuntos($billetera->getPuntos() - $cantidad);
                $em->persist($billetera);
                $em->persist($entidadDeCaridad);
                $em->flush();
                $this->get('minsaludba.avisos')->addSuccess('Se donó tu dinero correctamente !');
            }
            else{
                $this->get('minsaludba.avisos')->addError('No posees suficientes puntos para realizar la donación!');
            }
        }

        return $this->render('billetera/donar.html.twig', array(
            'billetera' => $billetera,
            'form' =>$form->createView(),
            'dineroPorPuntos' => $dineroPorPuntos,
            'puntosNecesarios' => $puntosNecesarios
        ));
    }
}
