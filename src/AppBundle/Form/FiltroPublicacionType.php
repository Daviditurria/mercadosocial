<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;


class FiltroPublicacionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', 'text', array(
                'label'=> 'Nombre'))
            ->add('descripcion', 'text', array(
                'label'=> 'Descripción'))
            ->add('producto', 'entity', array(
                'label'=> 'Producto',
                'class' => 'AppBundle:Producto',
                'empty_value'=>'-Seleccione-'
            ))
            ->add('precio', 'text', array(
                'label'=> 'Precio'))
            ->add('stock', 'text', array(
                'label'=> 'Stock'))
            ->add('categoria', 'entity', array(
                'label'=> 'Categoría',
                'class' => 'AppBundle:Categoria',
                'empty_value'=>'-Seleccione-'
            ))
            ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_filtropublicaciontype';
    }


}
