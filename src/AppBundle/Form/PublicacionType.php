<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;


class PublicacionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('foto', FileType::class, array(
            'required'=>false))
            ->add('nombre')
            ->add('descripcion')
            ->add('precio')
            ->add('stock')
            ->add('precioMaximo')
            ->add('precioMinimo')
            ->add('precioActual', 'choice', array(
                'choices' => array(
                    1 => 'Precio Mínimo',
                    2 => 'Precio estándar',
                    3 => 'Precio Máximo'
                ),
                'placeholder' => '-Seleccione-'
            ))
            ->add('estado', 'choice', array(
                'choices' => array(
                    'activa' => 'Activo',
                    'inactiva' => 'Inactivo',
                    'sin stock' => 'Sin stock'
                ),
                'placeholder' => '-Seleccione-'
            ))
            ->add('producto', 'entity', array(
                'label' => 'Producto',
                'class' => 'AppBundle:Producto',
                'placeholder' => '-Seleccione-',
                'required' => true
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Publicacion'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_publicacion';
    }


}
